%include "src/lib.inc"

global find_word

%define PTR_SIZE 8
%define NULL 0

section .text
; params:
;   rdi - str ptr
;   rsi - list ptr
; returns:
;   rax - matched value from list
find_word:
    enter 2, 0  ; create stack frame
.loop:
    cmp rsi, NULL
    je .no_entry

    add rsi, PTR_SIZE  ; point to key
    push rdi  ; save regs
    push rsi
    call string_equals  ; match by key
    pop rsi  ; restore regs
    pop rdi
    sub rsi, PTR_SIZE  ; restore ptr

    test rax,rax
    jnz .found  ; key matches

    mov rsi, [rsi]  ; to the next entry
    jmp .loop
.no_entry:
    xor rax, rax
    jmp .exit
.found:
    mov rax, rsi
.exit:
    leave
    ret
