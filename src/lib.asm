global exit
global parse_int
global parse_uint
global print_char
global print_int
global print_newline
global print_newline_err
global print_string
global print_string_err
global print_uint
global read_char
global read_word
global string_copy
global string_equals
global string_length

%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define SYSCALL_READ  0
%define SYSCALL_WRITE 1
%define SYSCALL_EXIT  60
%define NULL_CHAR 0
%define ASCII_MINUS 45

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp BYTE [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

print_string_err:
    mov r9, STDERR
    jmp _print_string
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov r9, STDOUT
_print_string:
    push r9
    push rdi
    call string_length
    pop rdi
    pop r9
; r9 - дескриптор потока вывода
echo:
    mov rsi, rdi  ; start addr
    mov rdx, rax  ; length
    mov rax, SYSCALL_WRITE
    mov rdi, r9
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    mov r9, STDOUT
    jmp _print_char
print_char_err:
    mov r9, STDERR
_print_char:
    push rdi
    mov rdi, rsp
    mov rax, 1
    call echo
    pop rdi  ; dummy
    ret

print_newline_err:
    mov rdi, 0xA
    jmp print_char_err

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi  ; num
    mov rsi, 10   ; divider
    push word 0   ; end of string
    mov rdi, rsp
    sub rsp, 24   ; stack_len - null_char
.loop:
    xor rdx, rdx
    div rsi       ; num / 10
    or dl, '0'   ; rem digit to ascii
    dec rdi
    mov [rdi], dl  ; push to stack
    test rax, rax ; is zero
    jne .loop
    call print_string
    add rsp, 24 + 2 ; pushed 16 bits on stack
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
.compare_chars:
    mov byte dl, byte [rdi+rcx]
    cmp byte dl, byte [rsi+rcx]
    jne .end
    inc rcx
    cmp byte dl, NULL_CHAR
    jne .compare_chars
    inc rax
.end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp ; addr read to
    mov rdx, 1   ; number of bytes to read
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    dec rsi
.read_char:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    lea r8, [rax-9]
    cmp r8, 1        ; '\n' or '\t'
    jbe .is_word_end
    cmp al, ' '
    je .is_word_end
    cmp rdx, rsi
    ja .too_long
    mov byte [rdi+rdx], al
    test al, al
    jz .word_end
    inc rdx
    jmp .read_char
.is_word_end:
    test rdx, rdx
    jz .read_char
.word_end:
    mov rax, rdi
    ret
.too_long:
    xor rax, rax
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; accumulator
    xor rdx, rdx ; counter
    xor rcx, rcx ; digit
.read_digit:
    mov cl, byte [rdi+rdx]
    xor cl, '0'
    cmp cl, 9
    ja .end
    lea rax, [rax*4 + rax]  ; sum *= 5
    lea rax, [rax*2 + rcx]  ; sum *= 2 + digit
    inc rdx
    jmp .read_digit
.end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov cl, byte [rdi]
    cmp cl, ASCII_MINUS
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx
    je .end
    inc rdx
    neg rax
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax  ; string length
    ;rdi source
    ;rsi dest
    ;rdx buffer length
    .copy_char:
        cmp rax, rdx
        jae .overflow

        mov cl, byte [rdi+rax]
        mov byte [rsi+rax], cl
        inc rax
        cmp cl, 0
        jne .copy_char
        ret
    .overflow:
        mov rax, 0
        ret
