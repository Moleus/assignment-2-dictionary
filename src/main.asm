%include "src/colon.inc"
%include "src/lib.inc"
%include "src/dict.inc"
%include "src/words.inc"

global _start

%define BUFF_SIZE 256
%define PTR_SIZE 8

%macro print_error_and_exit 1
    mov rdi, %1
    call print_string_err
    mov rdi, 1
    call exit
%endmacro


section .rodata
prompt_msg: db 'Enter key: ', 0
input_fail_msg: db 'Key length must be between 0 and 256', 10, 0
nothing_match_msg: db 'Nothing matches provided key', 10, 0
key_match_msg: db 'Found matching key: ', 0
separator_msg: db ' - ', 0

section .bss
input_buffer: resb BUFF_SIZE

section .text
_start:
    mov rdi, prompt_msg
    call print_string

.read_input:
    mov rdi, input_buffer
    mov rsi, BUFF_SIZE
    call read_word
    test rax, rax
    jz .input_fail

    push rdx  ; save key length

.check_input:
    mov rdi, input_buffer ; provided key
    mov rsi, NEXT ; list ptr
    call find_word
    test rax, rax
    jz .nothing_match

.print_match:
    mov rdi, key_match_msg
    push rax
    call print_string
    pop rax

    add rax, PTR_SIZE  ; shift to key start addr
    mov rdi, rax
    push rdi
    call print_string

    mov rdi, separator_msg
    call print_string
    pop rdi

    pop rdx ; restore key length
    lea rdi, [rdi + rdx + 1] ; shift to value start addr
    call print_string
    call print_newline
    xor rdi, rdi
    call exit

.input_fail:
    print_error_and_exit input_fail_msg

.nothing_match:
    print_error_and_exit nothing_match_msg
